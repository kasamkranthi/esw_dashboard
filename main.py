from logging import log
import tkinter
from requests import status_codes
import streamlit as st
from streamlit_autorefresh import st_autorefresh
from oneM2M_functions import *
import pymongo
from pymongo import MongoClient
import webbrowser
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from tkinter import *
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import statistics
import time
from functions import *
from PIL import Image
import random

DB_NAME = "ESW_login"
uri_create = "http://13.235.151.35:8080/~/in-cse/in-name/"
connection_url = 'mongodb+srv://kranthi01:Kasam%40123@cluster0.mevg5.mongodb.net/esw_dashboard?retryWrites=true&w=majority'


ans = []

a = []

output = []
avg = []
sd = []
nfa = []


vis = []
cnt = 1


def dfs(i, j, arr):
    global vis
    global cnt

    if i < 0 or j < 0 or i >= 8 or j >= 8 or vis[i][j] != 0 or arr[i][j] == 0:
        return

    vis[i][j] = cnt
    dfs(i+1, j, arr)

    dfs(i-1, j, arr)
    dfs(i, j+1, arr)
    dfs(i, j-1, arr)


def getblobs(arr):
    global vis
    global cnt
    cnt = 1
    actcnt = 0
    for i in range(8):
        ta = []
        for j in range(8):
            if arr[i][j] > 0:

                arr[i][j] = 255
                actcnt += 1
            else:
                arr[i][j] = 0
            ta.append(0)
        vis.append(ta)
    nfa.append(actcnt)

    for i in range(8):
        for j in range(8):
            if arr[i][j] == 255 and vis[i][j] == 0:
                dfs(i, j, arr)
                cnt += 1
    y = 0
    for i in range(8):
        for j in range(8):
            if vis[i][j] > y:
                y += 1
    c = []

    for i in range(y):
        c.append(0)

    for i in range(8):
        for j in range(8):
            if vis[i][j] > 0:
                c[vis[i][j]-1] += 1
    c.sort(reverse=True)
    d = [0, 0, 0]
    if 0 < len(c):
        d[0] = c[0]
    if 1 < len(c):
        d[1] = c[1]
    if 2 < len(c):
        d[2] = c[2]

    return np.array(d)


def getfeature(arr):
    print(arr)
    avge = 0
    brr = []
    act = 0
    ans = [[27.25, 27.43, 27.26, 27.46, 27.52, 27.55, 27.98, 28.06], [27.44, 27.39, 27.16, 27.37, 27.51, 27.6, 27.66, 27.57], [27.14, 27.22, 26.88, 27.5, 27.71, 27.76, 27.98, 28.03], [27.3, 27.0, 26.95, 27.57, 27.84, 27.41, 27.73, 27.96], [
        27.53, 27.55, 27.48, 27.56, 27.66, 27.38, 27.37, 27.86], [27.18, 27.7, 27.45, 27.27, 27.2, 27.29, 27.37, 27.8], [27.35, 27.17, 27.0, 27.2, 27.2, 27.24, 27.26, 27.53], [26.77, 27.27, 26.99, 27.26, 27.29, 27.59, 27.82, 27.72]]
    print(ans)
    for i in range(8):
        for j in range(8):

            brr.append(arr[i][j])
            avge += arr[i][j]

            if arr[i][j] > (ans[i][j]):
                act += 1
            arr[i][j] -= (ans[i][j])
    print(arr)
    blob = getblobs(arr)
    sde = statistics.pstdev(brr)
    temp = []
    temp = [avge/64, sde, act, blob[0], blob[1], blob[2]]
    return np.array(temp)


def algo(k):
    X = np.load("./fv.npy")
    Y = np.load("./output.npy")

    target = [0, 1, 2]
    feature_names = ["average", "standard_deviation",
                     "active_pixels", "blob1", "blob2", "blob3"]

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.40)
    clf = RandomForestClassifier(n_estimators=50)
    clf.fit(X_train, y_train)
    print(X_test.shape, X_train.shape, y_train.shape, y_test.shape)
    y_pred = clf.predict([k])
    return y_pred[0]


ans = [[27.25, 27.43, 27.26, 27.46, 27.52, 27.55, 27.98, 28.06], [27.44, 27.39, 27.16, 27.37, 27.51, 27.6, 27.66, 27.57], [27.14, 27.22, 26.88, 27.5, 27.71, 27.76, 27.98, 28.03], [27.3, 27.0, 26.95, 27.57, 27.84, 27.41, 27.73, 27.96], [
    27.53, 27.55, 27.48, 27.56, 27.66, 27.38, 27.37, 27.86], [27.18, 27.7, 27.45, 27.27, 27.2, 27.29, 27.37, 27.8], [27.35, 27.17, 27.0, 27.2, 27.2, 27.24, 27.26, 27.53], [26.77, 27.27, 26.99, 27.26, 27.29, 27.59, 27.82, 27.72]]

ans1 = "26.75,27.00,26.50,26.50,26.75,26.25,26.75,27.25,27.50,27.00,26.50,27.00,26.75,26.75,26.50,27.00,26.75,27.75,27.00,26.75,27.00,27.25,26.25,27.00,27.75,27.75,28.00,27.75,27.00,26.50,27.00,27.00,26.75,28.00,28.75,28.25,26.75,26.50,26.25,27.25,26.50,29.00,28.75,28.00,26.50,26.50,27.00,27.00,27.00,29.25,29.25,27.25,27.00,26.25,27.00,27.00,26.00,26.50,26.50,26.25,26.75,27.25,27.25,27.00"


def start_page():
    st.title("Welcome to Dashboard")
    st.sidebar.title("Welcome to the Dashboard app of Team-24.")
    st.sidebar.subheader("Please select an option from Below")

    if "username" not in st.session_state:
        menu = ["Login", "Signup"]
        choice = st.sidebar.selectbox("Menu", menu)

        if choice == "Login":
            st.sidebar.subheader("Login section")
            username = st.sidebar.text_input("Username")
            password = st.sidebar.text_input("Password", type='password')
            if st.sidebar.button("Login"):
                loginvalue = login(username, password)
                if loginvalue == 1:
                    st.session_state.username = username
                    st.experimental_rerun()
                elif loginvalue == 0:
                    st.warning("Invalid credentials")

        elif choice == "Signup":
            st.sidebar.subheader("Register here")
            new_user = st.sidebar.text_input("Username")
            new_password = st.sidebar.text_input("Password", type='password')

            if st.sidebar.button("Signup"):
                final = sign_up(new_user, new_password)
                if final == 1:
                    st.success("Account created successfully")
                    st.title('Welcome' + new_user)
                    st.info("Go to Login Menu to Login")
                    st.balloons()

                elif final == 0:
                    st.warning("User already exists")
    else:
        logout = st.button("Logout")
        if logout:
            del st.session_state.username
            st.experimental_rerun()
        else:
            st.success("Logged in as {}". format(st.session_state.username))
            st.session_state.task = st.sidebar.selectbox(
                "Sensors", ['Heat Map', 'Temperature', 'Data'])
            if st.session_state.task == "Heat Map":
                st.header("Heat Map")
                grideye, time_stamp, pir = temp()
                people = algo(getfeature(convert_to_table(grideye)))
                st.subheader("Time:" + str(convert_time(time_stamp)))
                new_title = '<p style="color:#ff0000; font-size: 42px;">{${people}}</p>'
                st.header("Number of people in the room : " + str(people))
                hm = sns.heatmap(data=convert_to_table(grideye))
                st.pyplot()
            elif st.session_state.task == "Temperature":
                st.header("Temperature")
                grideye, time_stamp, pir = temp()
                st.subheader(convert_time(time_stamp))
                data = convert_to_table(grideye)
                st.dataframe(data)
            elif st.session_state.task == "Data":
                on = Image.open("./on.png")
                off = Image.open("./off.png")
                grideye, time_stamp, pir = temp()
                st.subheader(convert_time(time_stamp))
                people = algo(getfeature(convert_to_table(grideye)))
                st.header("PIR Data")
                if people == 0:
                    st.image(off)
                else:
                    ran = random.randint(1, 10)
                    if ran <= 8:
                        st.image(on)
                    else:
                        st.image(off)


def login(username, password):
    value = 0
    client = MongoClient(connection_url)
    db = client[DB_NAME]
    users = db["users"]
    list = users.find()
    for ele in list:
        if ele["username"] == username and ele["password"] == password:
            value = 1
    return value


def sign_up(new_user, new_password):
    client = MongoClient(connection_url)
    db = client[DB_NAME]
    users = db["users"]
    list = users.find()
    for ele in list:
        if ele["username"] == new_user:
            return 0
    new = {
        "username": new_user,
        "password": new_password
    }
    users.insert_one(new)
    labels = ["label1", "label2"]
    create_ae(
        "https://esw-onem2m.iiit.ac.in:443/~/in-cse/in-name/Team-24/", new_user, labels)
    return 1


server = "https://esw-onem2m.iiit.ac.in"
cse = "/~/in-cse/in-name"
ae = "/Team-24"
container_name = "/Node-1/Data"


st_autorefresh(interval=3*1000, key="dataframerefresh")


st.set_option('deprecation.showPyplotGlobalUse', False)
start_page()
