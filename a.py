import json
import requests
import numpy as np
import statistics
import pandas as pd

from numpy import asarray
from numpy import savetxt
ans=[]

a=[]

output=[]
avg=[]
sd=[]
nfa=[]



vis=[]
cnt=1
def dfs(i,j,arr):
    global vis
    global cnt
   
    
    if i<0 or j<0 or i>=8 or j>=8 or vis[i][j] != 0 or arr[i][j] == 0:
        return
    
    vis[i][j]=cnt
    dfs(i+1,j,arr)
    
    dfs(i-1,j,arr)
    dfs(i,j+1,arr)
    dfs(i,j-1,arr)


def getblobs(arr):
    global vis
    global cnt
    cnt=1
    actcnt=0
    for i in range(8):
        ta=[]
        for j in range(8):
            if arr[i][j]>0:
                
                arr[i][j]=255
                actcnt+=1
            else:
                arr[i][j]=0
            ta.append(0)
        vis.append(ta)
    nfa.append(actcnt)
    
    
    
    for i in range(8):
        for j in range(8):
            if arr[i][j]==255 and vis[i][j]==0:
                dfs(i,j,arr)
                cnt+=1
    y=0
    for i in range(8):
        for j in range(8):
            if vis[i][j]>y:
                y+=1
    c=[]

    for i in range(y):
        c.append(0)
    
    for i in range(8):
        for j in range(8):
            if vis[i][j]>0:
                c[vis[i][j]-1]+=1
    c.sort(reverse=True)
    d=[0,0,0]
    if 0<len(c):
        d[0]=c[0]
    if 1<len(c):
        d[1]=c[1]
    if 2<len(c):
        d[2]=c[2]

    return np.array(d)

    
def getfeature(arr):
    print(arr)
    avge=0
    brr=[]
    act=0
    ans=[[27.25, 27.43, 27.26, 27.46, 27.52, 27.55, 27.98, 28.06], [27.44, 27.39, 27.16, 27.37, 27.51, 27.6, 27.66, 27.57], [27.14, 27.22, 26.88, 27.5, 27.71, 27.76, 27.98, 28.03], [27.3, 27.0, 26.95, 27.57, 27.84, 27.41, 27.73, 27.96], [27.53, 27.55, 27.48, 27.56, 27.66, 27.38, 27.37, 27.86], [27.18, 27.7, 27.45, 27.27, 27.2, 27.29, 27.37, 27.8], [27.35, 27.17, 27.0, 27.2, 27.2, 27.24, 27.26, 27.53], [26.77, 27.27, 26.99, 27.26, 27.29, 27.59, 27.82, 27.72]]
    print(ans)
    for i in range(8):
        for j in range(8):
            
            brr.append(arr[i][j])
            avge+=arr[i][j]
            
            if arr[i][j]>(ans[i][j]+1.0):
                act+=1
            arr[i][j]-=ans[i][j]
    print(arr)
    blob=getblobs(arr)
    sde=statistics.pstdev(brr)
    temp=[]
    temp=[avge/64,sde,act,blob[0],blob[1],blob[2]]
    return np.array(temp)


ans=[[27.25, 27.43, 27.26, 27.46, 27.52, 27.55, 27.98, 28.06], [27.44, 27.39, 27.16, 27.37, 27.51, 27.6, 27.66, 27.57], [27.14, 27.22, 26.88, 27.5, 27.71, 27.76, 27.98, 28.03], [27.3, 27.0, 26.95, 27.57, 27.84, 27.41, 27.73, 27.96], [27.53, 27.55, 27.48, 27.56, 27.66, 27.38, 27.37, 27.86], [27.18, 27.7, 27.45, 27.27, 27.2, 27.29, 27.37, 27.8], [27.35, 27.17, 27.0, 27.2, 27.2, 27.24, 27.26, 27.53], [26.77, 27.27, 26.99, 27.26, 27.29, 27.59, 27.82, 27.72]]

print(getfeature(ans))



