import json
import requests
import numpy as np
import statistics
import pandas as pd

from numpy import asarray
from numpy import savetxt
ans = []
response = requests.get("http://13.232.244.116:9000/records/")

_resp = json.loads(response.text)
a = []

output = []
avg = []
sd = []
nfa = []


vis = []
cnt = 1


def dfs(i, j, arr):
    global vis
    global cnt

    if i < 0 or j < 0 or i >= 8 or j >= 8 or vis[i][j] != 0 or arr[i][j] == 0:
        return

    vis[i][j] = cnt
    dfs(i+1, j, arr)

    dfs(i-1, j, arr)
    dfs(i, j+1, arr)
    dfs(i, j-1, arr)


def getblobs(arr):
    global vis
    global cnt
    cnt = 1
    vis = []
    actcnt = 0
    for i in range(8):
        ta = []
        for j in range(8):
            if arr[i][j] > 0:
                arr[i][j] = 255
                actcnt += 1
            else:
                arr[i][j] = 0
            ta.append(0)
        vis.append(ta)
    nfa.append(actcnt)

    for i in range(8):
        for j in range(8):
            if arr[i][j] == 255 and vis[i][j] == 0:
                dfs(i, j, arr)
                cnt += 1
    y = 0
    for i in range(8):
        for j in range(8):
            if vis[i][j] > y:
                y += 1
    c = []

    for i in range(y):
        c.append(0)

    for i in range(8):
        for j in range(8):
            if vis[i][j] > 0:
                c[vis[i][j]-1] += 1
    c.sort(reverse=True)
    d = [0, 0, 0]
    if 0 < len(c):
        d[0] = c[0]
    if 1 < len(c):
        d[1] = c[1]
    if 2 < len(c):
        d[2] = c[2]

    return np.array(d)


def generatetraining():
    zero_response = requests.get("http://13.232.244.116:9000/records/")
    one_response = requests.get("http://13.232.244.116:9000/records/One")
    two_response = requests.get("http://13.232.244.116:9000/records/Two")
   # three_response = requests.get("http://13.232.244.116:9000/records/Three")

    zeror = json.loads(zero_response.text)
    oner = json.loads(one_response.text)
    twor = json.loads(two_response.text)
    #threer = json.loads(three_response.text)

    grid = []

    for x in zeror:
        stdarr = []
        avgval = 0
        output.append(0)
        temp = []
        temp = x['grideye'].split(",")
        k = []
        for i in range(8):
            t = []
            for j in range(8):
                stdarr.append(float(temp[8*i+j]))
                avgval += float(temp[8*i+j])
                t.append(float(temp[8*i+j])-ans[i][j])
            k.append(t)
        avgval = avgval/64
        sd.append(statistics.pstdev(stdarr))
        avg.append(avgval)
        grid.append(k)

    l = 0
    for x in oner:
        l += 1
        avgval = 0
        stdarr = []
        output.append(1)
        temp = []
        temp = x['grideye'].split(",")
        k = []
        for i in range(8):
            t = []
            for j in range(8):
                stdarr.append(float(temp[8*i+j]))
                avgval += float(temp[8*i+j])
                t.append(float(temp[8*i+j])-ans[i][j])
            k.append(t)
        avg.append(avgval/64)
        sd.append(statistics.pstdev(stdarr))
        grid.append(k)

    l = 0
    for x in twor:
        avgval = 0
        l += 1
        stdarr = []
        output.append(2)

        temp = []
        temp = x['grideye'].split(",")
        k = []
        for i in range(8):
            t = []
            for j in range(8):
                stdarr.append(float(temp[8*i+j]))
                avgval += float(temp[8*i+j])
                t.append(float(temp[8*i+j])-ans[i][j])
            k.append(t)
        avg.append(avgval)
        sd.append(statistics.pstdev(stdarr))
        grid.append(k)

    """ l = 0
    for x in threer:
        avgval = 0
        l += 1
        stdarr = []
        output.append(3)

        temp = []
        temp = x['grideye'].split(",")
        k = []
        for i in range(8):
            t = []
            for j in range(8):
                stdarr.append(float(temp[8*i+j]))
                avgval += float(temp[8*i+j])
                t.append(float(temp[8*i+j])-ans[i][j])
            k.append(t)
        avg.append(avgval)
        sd.append(statistics.pstdev(stdarr))
        grid.append(k)
 """
    # for x in threer:
    #     output.append(3)
    #     temp=[]
    #     temp=x['grideye'].split(",")
    #     k=[]
    #     for i in range(8):
    #         t=[]
    #         for j in range(8):
    #             t.append(float(temp[8*i+j])-ans[i][j])
    #         k.append(t)
    #     grid.append(k)

    return grid


for x in _resp:
    temp = []
    temp = x['grideye'].split(",")
    for i in range(64):
        temp[i] = float(temp[i])
    a.append(temp)
fin = a[0]


for i in range(64):
    fin[i] = 0.0


for j in range(64):
    for i in range(300):
        fin[j] += a[i][j]


for i in range(64):
    fin[i] = fin[i]/300+1.0
    fin[i] = float("{:.2f}".format(fin[i]))

print(type(fin[0]))

for i in range(8):
    t = []
    for j in range(8):
        t.append(fin[8*i+j])
    ans.append(t)


iy = generatetraining()
blobs = []

for i in range(len(iy)):
    blobs.append(getblobs(iy[i]))
# print(nfa)
# print(avg)
# print(sd)
# print(len(sd))
# print(len(nfa))
# print(len(avg))
# print(len(output))
# print(len(blobs))

fv = []
for i in range(len(nfa)):
    temp = []
    temp.append(avg[i])
    temp.append(sd[i])
    temp.append(nfa[i])
    temp.append(blobs[i][0])
    temp.append(blobs[i][1])
    temp.append(blobs[i][2])
    fv.append(np.array(temp))
fv = np.array(fv)
np.save('fv.npy', fv)
np.save('output.npy', output)


print(fv)
