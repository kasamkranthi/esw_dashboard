from sklearn import datasets
import numpy as np
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
from sklearn.model_selection import train_test_split
X=np.load("./fv.npy")
Y=np.load("./output.npy")

target=[0,1,2,3]
feature_names=["average","standard_deviation","active_pixels","blob1","blob2","blob3"]

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size = 0.40)
clf = RandomForestClassifier(n_estimators = 100)
clf.fit(X_train, y_train)
print(X_test.shape,X_train.shape,y_train.shape,y_test.shape)
y_pred = clf.predict(X_test)
print(y_pred)
print("ACCURACY OF THE MODEL: ", metrics.accuracy_score(y_test, y_pred))