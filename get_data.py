from oneM2M_functions import *
import requests
import json

""" server = "http://127.0.0.1:8080"
cse = "/~/in-cse/in-name/"
ae = "Test-1"
container_name = "Data" """

# ------------------------------------------
# Fill code here to get latest content_instance
# specified by the URI
# ------------------------------------------
""" ret_code, latest_data = get_data(server+cse+ae+"/Data/la")
print(latest_data) """

uri = "http://13.232.244.116:9000/records/"
response = requests.get(uri)
_resp = json.loads(response.text)
print(_resp)
# ------------------------------------------

# ------------------------------------------
# Fill code here to get oldest content_instance
# specified by the URI
# ------------------------------------------
# ret_code, oldest_data = get_data(server+cse+ae+"/Data/ol")
# print(oldest_data)
# ------------------------------------------

# ------------------------------------------
# Fill code here to get all content_instances
# specified by the URI
# Note: change the return statement as given in get_data function inside the oneM2M_functions.py
# ------------------------------------------
# ret_code, all_data = get_data(server+cse+ae+"/Data?rcn=4")
# print(all_data)
# ------------------------------------------
""" with header:
    st.title("Welcome to Dashboard")
    ret_code, latest_data = temp()
    st.write(ret_code)
    st.write(latest_data) """
""" login() """
'''
import streamlit as st
from oneM2M_functions import *
import pymongo
from pymongo import MongoClient

DB_NAME = "ESW_login"
connection_url = 'mongodb://127.0.0.1:27017/'


def start_page():
    st.title("Welcome to the Dashboard app of Team-24.")
    st.subheader("Please select an option from sidebar")

    menu = ["Home", "Login", "Signup"]
    choice = st.sidebar.selectbox("Menu", menu)

    if choice == "Home":
        st.subheader("Home")

    elif choice == "Login":
        st.subheader("Login section")
        username = st.sidebar.text_input("Username")
        password = st.sidebar.text_input("Password", type='password')
        if st.sidebar.checkbox("Login"):
            loginvalue = login(username, password)
            if loginvalue == 1:
                st.success("Logged in as {}". format(username))
                task = st.selectbox("Task", ["Grideye", "PIR", "ESP"])
                if task == "Grideye":
                    st.subheader("Grideye")
                    st.subheader(username)
                elif task == "PIR":
                    st.subheader("PIR")
                    st.subheader(username)
                elif task == "ESP":
                    st.subheader("ESP")
                    st.subheader(username)
            elif loginvalue == 0:
                st.warning("Invalid credentials")

    elif choice == "Signup":
        st.subheader("Register here")
        new_user = st.text_input("Username")
        new_password = st.text_input("Password", type='password')

        if st.button("Signup"):
            final = sign_in(new_user, new_password)
            if final == 1:
                st.success("Account created successfully")
                st.info("Go to Login Menu to Login")

            elif final == 0:
                st.warning("User already exists")


def login(username, password):
    value = 0
    client = MongoClient(connection_url)
    db = client[DB_NAME]
    users = db["users"]
    list = users.find()
    for ele in list:
        if ele["username"] == username and ele["password"] == password:
            value = 1
    return value


def sign_in(new_user, new_password):
    client = MongoClient(connection_url)
    db = client[DB_NAME]
    users = db["users"]
    list = users.find()
    for ele in list:
        if ele["username"] == new_user:
            return 0
    new = {
        "username": new_user,
        "password": new_password
    }
    users.insert_one(new)
    return 1


server = "http://13.235.151.35:8080"
cse = "/~/in-cse/in-name"
ae = "/Test1"
container_name = "/Grideye"


def temp():
    ret_code, latest_data = get_data(server+cse+ae+container_name+"/la")
    return ret_code, latest_data


def convert_hex(green):
    temp = hex(green)
    final = "#ff"
    if len(temp) == 4:
        final = final + temp[2:]
    else:
        final = final + "0" + temp[2:]
    final = final + "00"
    return final


def back_color(url, back):
    st.markdown(
        f'<p style="background-color:{back};font-size:24px;border-radius:2%;">{url}</p>', unsafe_allow_html=True)


header = st.container()
dataset = st.container()
features = st.container()
modelTraining = st.container()


# start_page()
text = """
   1"""
back_color(text, convert_hex(0))
temp = (hex(255))
st.write(temp[2:])
st.write(convert_hex(0))

'''
